import { Action } from '@ngrx/store';
import { Effect } from '@ngrx/effects';

import { Book } from './book.model';

export interface BookState {
    books: Book[];
    favorite: Book;
}

export const initBookState = function () {
    return {
        books: [],
        favorite: null
    }
}

export enum BookActionTypes {
    ADD_BOOK = "[Book] Add Book",
    REMOVE_BOOK = "[Book] Remove Book",
    SELECT_FAVORITE_BOOK = "[Book] Select Favorite",
    LIKE_BOOK = "[Book] Like",
    DISLIKE_BOOK = "[Book] Dislike"
}

export class AddBook implements Action {
    type = BookActionTypes.ADD_BOOK;
    constructor(public book: Book) { }
}

export class RemoveBook implements Action {
    type = BookActionTypes.REMOVE_BOOK;
    constructor(public book: Book) { }
}

export class SelectFavoriteBook implements Action {
    type = BookActionTypes.SELECT_FAVORITE_BOOK;
    constructor(public book: Book) { }
}

export class LikeBook implements Action {
    type = BookActionTypes.LIKE_BOOK;
    constructor(public book: Book) { }
}

export class DislikeBook implements Action {
    type = BookActionTypes.DISLIKE_BOOK;
    constructor(public book: Book) { }
}

export type BookActions =
    AddBook | RemoveBook | SelectFavoriteBook | LikeBook | DislikeBook;

export function bookReducers(state: BookState, action: BookActions): BookState {
    switch(action.type) {
        case BookActionTypes.ADD_BOOK: {
            return {
                ...state,
                books: [...state.books, (action as AddBook).book]
            }
        }
        case BookActionTypes.REMOVE_BOOK: {
            const flag = (action as RemoveBook).book;
            const index = state.books.findIndex(b =>  b === flag);
            if(index > -1) {
                state.books.splice(index, 1);
            }
            if(flag === state.favorite) {
                state.favorite = null;
            }
            return {
                ...state
            }
        }
        case BookActionTypes.SELECT_FAVORITE_BOOK: {
            const book = (action as SelectFavoriteBook).book;
            state.books.forEach(b => b.setIsfavorite(false));
            book.setIsfavorite(true);
            return {
                ...state,
                favorite: book
            }
        }
        case BookActionTypes.LIKE_BOOK: {
            const flag = (action as LikeBook).book;
            state.books
            .find(b => b === flag)
            .addLike();
            return {
                ...state
            }
        }
        case BookActionTypes.DISLIKE_BOOK: {
            const flag = (action as DislikeBook).book;
            state.books
            .find(b => b === flag)
            .addDislike();
            return {
                ...state
            }
        }
    }
    return state;
}