export class Book {

    private isFavorite: boolean;

    private likes: number;

    private dislikes: number;

    constructor(public title: string, public isbn: string, public stock: number) {
        this.isFavorite = false;
        this.likes = 0;
        this.dislikes = 0;
    }

    addLike() {
        this.likes++;
    }

    addDislike() {
        this.dislikes++;
    }

    setIsfavorite(isFavorite: boolean) {
        this.isFavorite = isFavorite;
    }

    getIsFavorite(): boolean {
        return this.isFavorite;
    }

    getLikes(): number {
        return this.likes;
    }

    getDislikes(): number {
        return this.dislikes;
    }
}