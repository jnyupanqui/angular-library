import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Book } from 'src/app/models/book.model';
import { BookService } from 'src/app/services/book.service';
import { isNumber } from 'util';

export enum StockStatus {
  DISPONIBILIDAD_INMEDIATA = "Disponibilidad inmediata.",
  NO_DISPONIBLE = "No disponible.",
  CONSULTAR_STOCK = "Consultar stock."
}

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit {

  @Input()
  book: Book;

  @Output()
  eventSelectFavorite: EventEmitter<Book>;

  @Output()
  eventRemoveBook: EventEmitter<Book>;

  @Output()
  eventLike: EventEmitter<Book>;

  @Output()
  eventDislike: EventEmitter<Book>;

  constructor(private bookService: BookService) {
    this.eventSelectFavorite = new EventEmitter();
    this.eventRemoveBook = new EventEmitter();
    this.eventLike = new EventEmitter();
    this.eventDislike = new EventEmitter();
  }

  ngOnInit(): void {
  }

  selectFavorite() {
    this.eventSelectFavorite.emit(this.book);
  }

  removeBook() {
    this.eventRemoveBook.emit(this.book);
  }

  like() {
    this.eventLike.emit(this.book);
  }

  dislike() {
    this.eventDislike.emit(this.book);
  }

  getStockStatus(): {
    message: string,
    color: string
  } {
    if (isNaN(this.book.stock)) {
      return {
        message: StockStatus.CONSULTAR_STOCK,
        color: 'orange'
      }
    }
    if (this.book.stock > 0) {
      return {
        message: StockStatus.DISPONIBILIDAD_INMEDIATA,
        color: 'green'
      }
    }
    return {
      message: StockStatus.NO_DISPONIBLE,
      color: 'red'
    }
  }
}
