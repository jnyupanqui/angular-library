import { Component, OnInit } from '@angular/core';
import { Book } from '../models/book.model';
import { BookService } from '../services/book.service';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  books: Book[];

  favorite: Book;

  constructor(private bookService: BookService) {
    this.books = [];
    this.favorite = null;
  }

  ngOnInit(): void {
    this.bookService.subscribeToBooks().subscribe(books => {
      if(books != null) {
        this.books = books;
      }
    });

    this.bookService.subscribeToFavorite().subscribe(favorite => {
      if(favorite != null) {
        this.favorite = favorite;
      }
    });
  }

  addBook(book: Book) {
    this.bookService.addBook(book);
  }

  removeBook(book: Book) {
    this.bookService.removeBook(book);
  }

  setFavorite(book: Book) {
    this.bookService.setFavorite(book);
  }

  like(book: Book) {
    this.bookService.addLike(book);
  }

  dislike(book: Book) {
    this.bookService.addDislike(book);
  }

}
