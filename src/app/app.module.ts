import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AddBookFormComponent } from './add-book-form/add-book-form.component';
import { BookListComponent } from './book-list/book-list.component';
import { BookDetailsComponent } from './book-list/book-details/book-details.component';

import { RouterModule, Routes } from '@angular/router';
import { BookService } from './services/book.service';
import { BookState, bookReducers, initBookState } from './models/book.state.model';
import { StoreModule as NGRXStoreModule, ActionReducerMap } from '@ngrx/store';

export interface AppState {
  book: BookState
}

const reducers: ActionReducerMap<AppState> = {
  book: bookReducers
}

let initState = {
  book: initBookState()
}

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AddBookFormComponent,
    BookListComponent,
    BookDetailsComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    RouterModule.forRoot(routes),
    NGRXStoreModule.forRoot(reducers, { initialState: initState ,runtimeChecks: {
      strictActionImmutability: false,
      strictStateImmutability: false
    }})
  ],
  providers: [
    BookService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
