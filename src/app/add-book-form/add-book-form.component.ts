import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BookService } from '../services/book.service';
import { Book } from '../models/book.model';
import { FormGroup, FormBuilder, Validators, ValidatorFn, FormControl } from '@angular/forms';
import validator from 'validator';

@Component({
  selector: 'app-add-book-form',
  templateUrl: './add-book-form.component.html',
  styleUrls: ['./add-book-form.component.css']
})
export class AddBookFormComponent implements OnInit {

  @Output()
  eventAddBook: EventEmitter<Book>;

  fg: FormGroup;

  titleMinLength = 3;

  constructor(fb: FormBuilder) {
    this.eventAddBook = new EventEmitter();

    this.fg = fb.group({
      inputTitle: ['', Validators.compose([
        Validators.required,
        this.minLengthValidator(this.titleMinLength),
        this.noEmptyValidator
      ])],
      inputIsbn: ['', Validators.required],
      inputStock: ['',  this.numberOrEmptyOrBlankValidator]
    });  

   }

  ngOnInit(): void {
  }

  addBook(title: string, isbn: string, stock: string) {
    const book = new Book(title, isbn, parseInt(stock));
    this.eventAddBook.emit(book);
  }

  minLengthValidator(minLength: number): ValidatorFn {
    return (control: FormControl): { [s: string]: boolean } | null => {
      const inputLength = control.value.toString().trim().length;
      if (inputLength < minLength) {
        return { minLength: true };
      }
      return null;
    }
  }

  isbnValidator(control: FormControl): { [s: string]: boolean } {
    const isbn = control.value.toString().trim();
    if(!validator.isISBN(isbn)) {
      return { isbnInvalid: true }
    }
    return null;
  }

  noEmptyValidator(control: FormControl): { [s: string]: boolean } {
    const isEmpty = control.value.toString().trim().length == 0;
    if(isEmpty) {
      return { numberOrEmptyInvalid: true };
    }
    return null;
  }

  numberOrEmptyOrBlankValidator(control: FormControl): { [s: string]: boolean } {
    const isNumber = !isNaN(parseInt(control.value));
    const isEmptyOrBlank = control.value.toString().trim().length == 0;
    if(!isNumber && !isEmptyOrBlank) {
      return { numberOrEmptyInvalid: true };
    }
    return null;
  }

  inputIsbnfocusOut() {
    const isbnControl = this.fg.controls['inputIsbn'];
    isbnControl.setValidators([this.isbnValidator]);
    isbnControl.updateValueAndValidity();
  }


}
