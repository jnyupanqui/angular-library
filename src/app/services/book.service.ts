import { Injectable } from '@angular/core';
import { Book } from '../models/book.model';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { AppState } from '../app.module';
import { Store } from '@ngrx/store';
import { AddBook, SelectFavoriteBook, LikeBook, DislikeBook, RemoveBook } from '../models/book.state.model';

@Injectable()
export class BookService {

    books: Book[] = [];

    favorite: Book = null;

    booksSubject: Subject<Book[]> = new BehaviorSubject(null);

    favoriteSubject: Subject<Book> = new BehaviorSubject(null);

    constructor(private store: Store<AppState>) {
        this.store.select(state => state.book.books)
            .subscribe(books => {
                if (books != null) {
                    this.books = books;
                    this.booksSubject.next(this.books);
                }
            });
        this.store.select(state => state.book.favorite)
            .subscribe(favorite => {
                if (favorite != null) {
                    this.favorite = favorite;
                    this.favoriteSubject.next(this.favorite);
                }
            });

    }

    addBook(book: Book) {
        this.store.dispatch(new AddBook(book));
    }

    subscribeToBooks(): Observable<Book[]> {
        return this.booksSubject.asObservable();
    }

    subscribeToFavorite(): Observable<Book> {
        return this.favoriteSubject.asObservable();
    }

    setFavorite(book: Book) {
        this.store.dispatch(new SelectFavoriteBook(book));
    }

    addLike(book: Book) {
        this.store.dispatch(new LikeBook(book));
    }

    addDislike(book: Book) {
        this.store.dispatch(new DislikeBook(book));
    }

    removeBook(book: Book) {
        this.store.dispatch(new RemoveBook(book));
    }
}